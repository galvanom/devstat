package com.devstat;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Vacancy> vacancies = new LinkedList<>();
//        Vacancy vac1 = new Vacancy();
//        Vacancy vac2 = new Vacancy();
//        vac1.setVacancyId(12345);
//        vac1.setVacancyName("C++ developer 1");
//        vac1.setLangName("C/C++");
//        vac1.setKeySkills(new String[]{});
//        vac2.setVacancyId(6789);
//        vac2.setVacancyName("C++ developer 2");
//        vac2.setLangName("C/C++");
//        vac2.setKeySkills(new String[]{"C++ Builder", "SQL"});
//        vacancies.add(vac1);
//        vacancies.add(vac2);
        DataBase db = DataBase.getInstance();
        String[] languages = {"Java","PHP","Python","1C","Android","IOS","Frontend",".NET","C/C++"};
//        db.insert(vacancies);
//        String[] array = {"Java","PL SQL","Ho ba boo ba"};
//        System.out.println(Arrays.toString(array).replace("[", "{").replace(']','}'));
//        vacancies = new Crawler().getAllVacancies();
        for (String language : languages) {
            vacancies = new Crawler().getVacanciesByLangId(language);
            db.insert(vacancies);
        }


//        for (Vacancy vacancy : vacancies){
//            System.out.println(vacancy);
//        }
//        Map<String,String> vacancyId = new HashMap<>();
//        vacancyId.put("16006920", "Java");
//        vacancyId.put("19886422", "C/C++");
//        vacancyId.put("19946628", "C/C++");
//        vacancyId.put("17456206","Java");
//        vacancyId.put("19884937","Java");
//        vacancyId.put("20122297","Java");
//
//        Class crawlerClass = Crawler.class;
//        Crawler crawlerTest = new Crawler();
//        try {
//            Method getVacancy = crawlerClass.getDeclaredMethod("getVacancy", String.class, String.class);
//            getVacancy.setAccessible(true);
//            for (Map.Entry entry : vacancyId.entrySet()) {
//                Vacancy vacancy = (Vacancy) getVacancy.invoke(crawlerTest, entry.getKey(), entry.getValue());
//                System.out.println(vacancy.toString());
//                vacancies.add(vacancy);
//            }
//            db.insert(vacancies);
//        }
//        catch(Exception e){
//            e.printStackTrace();
//        }

        Aggregator aggregator = new Aggregator();
        Date today = new Date();
        Date daysAgo = getSubDate(today, 30);
        SimpleDateFormat dayAndTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat justDate = new SimpleDateFormat("dd.MM.yyyy");
        List<String[]> table = aggregator.buildData(
                dayAndTime.format(daysAgo),
                dayAndTime.format(today),
                languages);
        aggregator.writeToFile(table, justDate.format(today), justDate.format(daysAgo), "data.js");
        for (String line[] : table){
            System.out.println(Arrays.toString(line));
        }


//        System.out.println(getSubDate(new Date(), 30));
    }
    private static Date getSubDate(Date date, int daysNumber){
        GregorianCalendar greg = new GregorianCalendar();
        greg.setTime(date);
        greg.add(Calendar.DATE, -daysNumber);
        return greg.getTime();
    }
}
