package com.devstat;

import java.io.*;
import java.net.MalformedURLException;
import java.util.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.ws.http.HTTPException;

public class Crawler{
	private static final Map<String, String> techRequests = new HashMap<>();
	private static final String API_URL = "https://api.hh.ru/";
	private static final List<Integer> areas = new LinkedList<>();
	static {
		// The way languages are represented in DB:
		// {Java,PHP,Python,1C,Android,IOS,Frontend,.NET,DB,C/C++}
		String programmerNames = "(Developer+OR+программист+OR+разработчик)";
		techRequests.put("Java", "text=Name:((Java+And+NOT+Javascript+AND+NOT+Android)+AND+"+programmerNames+")"); // [V]
		techRequests.put("C/C++", "text=Name:(C%2B%2B+And+NOT+C%23+AND+" + programmerNames+")"); // [V]
		techRequests.put("Android", "text=Name:(Android+And+" + programmerNames+")"); // [V]
		techRequests.put("IOS", "text=Name:((IOS+OR+Swift+OR+Objective)+And+" + programmerNames+")"); // [V]
		techRequests.put(".NET", "text=Name:((C%23+OR+ASP.NET+OR+.Net)+And+"+programmerNames+")"); // [V]
		techRequests.put("Python", "text=Name:((Python+OR+Django)+And+"+programmerNames+")"); // [V]
		techRequests.put("PHP", "text=Name:((PHP+OR+Bitrix+OR+Битрикс+OR+Битрикс24)+And+"+programmerNames+")"); // [V]
		techRequests.put("Frontend", "text=Name:((JS+OR+Javascript+OR+Frontend+OR+node.js)+And+" +
				"(NOT+Java+And+NOT+C%2B%2B+And+NOT+Android+And+NOT+IOS)+And+"+programmerNames+")"); // [V]
		techRequests.put("1C", "text=Name:((1С+OR+1C)+And+"+programmerNames+"+AND+NOT+Битрикс)"); // [V]
//		techRequests.put("DB","text=Name:((БД+OR+Баз+данных) "+programmerNames+")" );
		System.out.println(techRequests.toString());
		areas.add(1); // Moscow
		areas.add(2); // Saint Petersburg
	}
	public List<Vacancy> getVacanciesByLangId(String langId){
		ExecutorService threadService = Executors.newFixedThreadPool(4);
		List<Future<Vacancy>> results;
		List<Callable<Vacancy>> vacanciesJobs = new LinkedList<>();
		List<Vacancy> vacancies = new LinkedList<>();
		if (techRequests.containsKey(langId)) {
			System.out.printf("Started the language %s ...\n", langId);
			for (Integer area : areas) {
				System.out.printf("Area %d ...\n", area);
				String langName = langId;
				String parameters = techRequests.get(langId);
				List<String> vacanciesId = getVacanciesId(parameters, area);
				for (String id : vacanciesId) {
					vacanciesJobs.add(new Callable<Vacancy>() {
						@Override
						public Vacancy call() throws Exception {
							return getVacancy(id, langName);
						}
					});
				}
			}
		}
		try {
			results = threadService.invokeAll(vacanciesJobs);
			for (Future<Vacancy> result : results){
				vacancies.add(result.get());
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		finally {
			threadService.shutdown();
		}

		return vacancies;
	}
	public List<Vacancy> getAllVacancies(){
		List<Vacancy> vacancies = new LinkedList<>();
		for (Map.Entry<String, String> entry : techRequests.entrySet()) {
			vacancies.addAll(getVacanciesByLangId(entry.getKey()));
		}
	    return vacancies;
    }
	private List<String> getVacanciesId(String parameters, int areaId){
		List<String> vacanciesId = new LinkedList<>();
		int pagesLeft = 0;
		int currentPage = 0;
		int reconnectionsNumber = 3;
		System.out.printf("URL parameters: %s\n",API_URL+"vacancies?"+parameters);
		try{
			do {
				String url = API_URL + "vacancies?" + parameters + "&page=" + currentPage + "&area=" + areaId;
				URL urlObj = new URL(url);
				HttpsURLConnection connection = (HttpsURLConnection) urlObj.openConnection();
				connection.setRequestMethod("GET");
				connection.setRequestProperty("User-Agent", "DevStatApp (devstat@devstat.com)");
				int responseCode = connection.getResponseCode();
				if (responseCode == HttpURLConnection.HTTP_OK) {
					InputStreamReader jsonReader = new InputStreamReader(connection.getInputStream());
					pagesLeft = getVacanciesFromPage(jsonReader, vacanciesId);
					jsonReader.close();
				}
				else{
					System.out.println("Response code differs from 200: " + responseCode);
//					InputStreamReader jsonReader = new InputStreamReader(connection.getInputStream());
//					System.out.println(jsonReader);
					if (reconnectionsNumber > 0){
						reconnectionsNumber--;
						connection.disconnect();
						continue;
					}

				}
				connection.disconnect();
				currentPage++;
			} while(reconnectionsNumber > 0 && pagesLeft > 0);
		}
		catch (Exception e){
			System.out.println(e);
		}
//		System.out.printf("Total vacancies: %d\n", vacanciesId.size());
		return vacanciesId;
	}

	private int getVacanciesFromPage(Reader in, List<String> ids){
		int pagesLeft = 0;
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(in);
			JSONArray itemsList = (JSONArray) jsonObject.get("items");
			long currentPage = (long) jsonObject.get("page");
			long totalPages = (long) jsonObject.get("pages");
			pagesLeft = (int)(totalPages - currentPage);

			// Get all vacancies on this page
			for (int j = 0; j < itemsList.size(); j++) {
				JSONObject item = (JSONObject) itemsList.get(j);
				ids.add((String) item.get("id"));
			}

		}
		catch (Exception e){
			e.printStackTrace();
		}
		return pagesLeft;
	}

	/**
	 * Get vacancy JSON and put it to vacancy object
	 * @param vacancyId represents vacancy unique identifier
     * @param langName is for language identification
	 * @return Filled vacancy object or an Empty one if an error occured
	 */
	private Vacancy getVacancy(String vacancyId, String langName){
		System.out.printf("Language: %s, Id: %s\n", langName, vacancyId);
		Vacancy vacancy = new Vacancy();
		String requestAddress = "https://api.hh.ru/vacancies/";
		InputStreamReader in = null;
		HttpsURLConnection connection = null;

		try {
			URL url = new URL(requestAddress + vacancyId);
			connection = (HttpsURLConnection) url.openConnection();
			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK){
				in = new InputStreamReader(connection.getInputStream());
				JSONParser parser = new JSONParser();
				JSONObject jsonObject = (JSONObject) parser.parse(in);
				if (jsonObject.containsKey("id") && jsonObject.containsKey("name")) {
					vacancy.setVacancyId(Long.parseLong((String) jsonObject.get("id")));
					vacancy.setVacancyName((String) jsonObject.get("name"));
					vacancy.setLangName(langName);
					if (jsonObject.containsKey("employer")){
						JSONObject employerInfo = (JSONObject)jsonObject.get("employer");
						if (employerInfo.containsKey("id")){
							vacancy.setEmployerId(Long.parseLong((String) employerInfo.get("id")));
						}
						if (employerInfo.containsKey("name")){
							vacancy.setEmployerName((String) employerInfo.get("name"));
						}
					}
					if (jsonObject.containsKey("area")){
						JSONObject areaInfo = (JSONObject)jsonObject.get("area");
						if (areaInfo.containsKey("id")){
							vacancy.setArea(Long.parseLong((String) areaInfo.get("id")));
						}
					}
//					"salary":{"to":null,"from":80000,"currency":"RUR"}
					if (jsonObject.containsKey("salary")){
						JSONObject salaryInfo = (JSONObject)jsonObject.get("salary");
						if (salaryInfo != null) {
							if (salaryInfo.containsKey("to")) {
								Long to = (Long) salaryInfo.get("to");
								if (to != null) {
									vacancy.setSalaryTo(to);
								}
							}
							if (salaryInfo.containsKey("from")) {
								Long from = (Long) salaryInfo.get("from");
								if (from != null) {
									vacancy.setSalaryFrom(from);
								}
							}
							if (salaryInfo.containsKey("currency")) {
								String currency = (String) salaryInfo.get("currency");
								if (currency != null) {
									vacancy.setSalaryCurrency(currency);
								}
							}
						}
					}
					if (jsonObject.containsKey("specializations")){
						JSONArray specArray = (JSONArray) jsonObject.get("specializations");
						JSONObject profInfo = (JSONObject) specArray.get(0);
						if (profInfo.containsKey("id")){
							vacancy.setSpecializationId(Double.parseDouble((String)profInfo.get("id")));
						}
					}
					if (jsonObject.containsKey("key_skills")){
						JSONArray skillsArray = (JSONArray) jsonObject.get("key_skills");
						if (skillsArray.size() > 0) {
							ArrayList<String> key_skills = new ArrayList<>();
							for (Object name : skillsArray) {
								key_skills.add((String) ((JSONObject) name).get("name"));
							}
							vacancy.setKeySkills(key_skills);
						}
					}
					if (jsonObject.containsKey("experience")){
						JSONObject experienceInfo = (JSONObject) jsonObject.get("experience");
						if (experienceInfo.containsKey("id")){
							vacancy.setExperience((String)experienceInfo.get("id"));
						}
					}
					if (jsonObject.containsKey("schedule")){
						JSONObject scheduleInfo = (JSONObject) jsonObject.get("schedule");
						if (scheduleInfo.containsKey("id")){
							vacancy.setSchedule((String)scheduleInfo.get("id"));
						}
					}
				}
			}
		}
		catch (MalformedURLException e){
			System.out.println("Bad url " + requestAddress + vacancyId +" " + e);
		}
		catch (ParseException e){
			System.out.println("Can't parse JSON from "+vacancyId + " " + e);
			return new Vacancy(); // return an empty object
		}
		catch (IOException e) {
			System.out.println("Can't get vacancy " +vacancyId + " " + e);
			return new Vacancy(); // return an empty object
		}
		finally {
			try {
				if (in != null) {
					in.close();
				}
			}
			catch (IOException e){
				System.out.println("Can't close input stream " + e);
			}
			if (connection != null){
				connection.disconnect();
			}
		}
		return vacancy;
	}
}