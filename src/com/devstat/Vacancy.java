package com.devstat;

import java.util.ArrayList;
import java.util.Arrays;

/*
"schedule":{"id":"fullDay","name":"Полный день"}
"experience":{"id":"between3And6","name":"От 3 до 6 лет"}
"area":{"url":"https://api.hh.ru/areas/2","id":"2","name":"Санкт-Петербург"}
"key_skills":[{"name":"Java"},{"name":"Spring Framework"},{"name":"Git"}]
"id":"19323712"
"salary":{"to":100000,"from":70000,"currency":"RUR"}
"name":"Java Web Developer"
"employer" : {"name":"Geoscan", "id":" "}

To check:
specializations":[{"profarea_id":"1","profarea_name":"Информационные технологии, интернет, телеком","id":"1.221","name":"Программирование, Разработка"}]}
 */
public class Vacancy {
    public final static int NO_ID = -1;
    private long vacancyId = NO_ID;
    private String vacancyName = "";
    private long employerId = -1;
    private String employerName = "";
    private double specializationId = -1.0;
    private long area = -1;
    private long salaryTo = -1;
    private long salaryFrom = -1;
    private String salaryCurrency = "";
    private ArrayList<String> keySkills = new ArrayList<>();
    private String experience = "";
    private String schedule = "";
    private String langName = "";

    public long getVacancyId() {
        return vacancyId;
    }

    public void setVacancyId(long vacancyId) {
        this.vacancyId = vacancyId;
    }

    public String getVacancyName() {
        return vacancyName;
    }

    public void setVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
    }

    public long getEmployerId() {
        return employerId;
    }

    public void setEmployerId(long employerId) {
        this.employerId = employerId;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public double getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(double specializationId) {
        this.specializationId = specializationId;
    }

    public long getArea() {
        return area;
    }

    public void setArea(long area) {
        this.area = area;
    }

    public long getSalaryTo() {
        return salaryTo;
    }

    public void setSalaryTo(long salaryTo) {
        this.salaryTo = salaryTo;
    }

    public long getSalaryFrom() {
        return salaryFrom;
    }

    public void setSalaryFrom(long salaryFrom) {
        this.salaryFrom = salaryFrom;
    }

    public String getSalaryCurrency() {
        return salaryCurrency;
    }

    public void setSalaryCurrency(String salaryCurrency) {
        this.salaryCurrency = salaryCurrency;
    }

    public ArrayList<String> getKeySkills() {
        return keySkills;
    }

    public void setKeySkills(ArrayList<String> keySkills) {
        this.keySkills = keySkills;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }

    @Override
    public String toString() {
        return vacancyId + " " + vacancyName + " " + employerId + " " + employerName + " " +
                specializationId + " " + area + " " + salaryFrom + " " + salaryTo + " " + salaryCurrency + " " +
                experience + " " + schedule + " " + langName + "\n" + Arrays.toString(keySkills.toArray());
    }
    public long getSalaryAvg(){
        long value = -1;
        if (salaryTo < 0 && salaryFrom < 0){
            value = -1;
        }
        else if (salaryTo > 0 && salaryFrom > 0){
            value = salaryFrom;
        }
        else if (salaryTo > 0 && salaryFrom < 0){
            value = salaryTo;
        }
        else if (salaryTo < 0 && salaryFrom > 0){
            value = salaryFrom;
        }
        return value;
    }
}
