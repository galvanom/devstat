package com.devstat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class Aggregator {
    private final int ROWS_NUMBER = 6;
    public void writeToFile(List<String[]> table, String dateTo, String dateFrom, String filePath){
        StringBuilder jsVars = new StringBuilder("var dataSet = " + getJSArray(table) + ";");
        jsVars.append("\nvar dateTo = \'").append(dateTo).append("\';");
        jsVars.append("\nvar dateFrom = \'").append(dateFrom).append("\';");
        System.out.println(jsVars.toString());
        try (FileOutputStream fos = new FileOutputStream(new File(filePath))){
            fos.write(jsVars.toString().getBytes());
        }
        catch (FileNotFoundException e){
            System.out.println("Can't write data table. File not found" +filePath + " " + e);
        }
        catch (IOException e){
            System.out.println("Can't write data table " + e);
        }
    }
    private String getJSArray(List<String[]> table){
        StringBuilder jsArray = new StringBuilder();
        jsArray.append("[");
        Iterator<String[]> tableIterator = table.iterator();
        while (tableIterator.hasNext()){
            jsArray.append(Arrays.toString(tableIterator.next()));
            if (tableIterator.hasNext()) {
                jsArray.append(", ");
            }
        }
        jsArray.append("]");
        return jsArray.toString();
    }

    public List<String[]> buildData(String fromDate, String toDate, String[] languages){
        List<String[]> table = new LinkedList<>();
        DataBase db = DataBase.getInstance();
        Map<String, String> experienceMap = new HashMap<>();
        experienceMap.put("between3And6", "от 3 до 6 лет");
        experienceMap.put("between1And3", "от 1 до 3 лет");
        experienceMap.put("moreThan6", "более 6 лет");
        experienceMap.put("noExperience", "без опыта");
        experienceMap.put(null, "нет данных");

//        Average Salary
        Map<String,Double> avgSalary = db.getAvgSalary(fromDate, toDate);
//        for (Map.Entry<String, Double> salary : avgSalary.entrySet()){
//            System.out.printf("Lang: %s Average salary: %d\n", salary.getKey(), salary.getValue().intValue());
//        }
//        Vacancies number
        Map<String, Integer> vacanciesNumber = db.getVacanciesNumber(fromDate, toDate);
//        for (Map.Entry<String, Integer> vacancies : vacanciesNumber.entrySet()){
//            System.out.printf("Lang: %s Vacancies number: %d\n", vacancies.getKey(), vacancies.getValue());
//        }
//        Remote jobs number
        Map<String, Integer> remoteJobs = db.getRemoteJobsNumber(fromDate, toDate);
//        for (Map.Entry<String, Integer> job : remoteJobs.entrySet()){
//            System.out.printf("Lang: %s Remote jobs number: %d\n", job.getKey(), job.getValue());
//        }
//        Common experience
        Map<String, String> experience = db.getExperience(fromDate, toDate);
//        for (Map.Entry<String, String> expEntry : experience.entrySet()){
//            System.out.printf("Lang: %s Most common experience: %s\n", expEntry.getKey(), expEntry.getValue());
//        }

        for (String language : languages){
            String[] languageLine = new String[ROWS_NUMBER];
            languageLine[0] = "\"" + language + "\"";
            languageLine[1] = String.valueOf(vacanciesNumber.get(language) == null ? 0 : vacanciesNumber.get(language));
            languageLine[2] = String.valueOf(avgSalary.get(language) == null ? 0 : avgSalary.get(language).intValue());
            languageLine[3] = "\"" + experienceMap.get(experience.get(language)) + "\"";
            languageLine[4] = String.valueOf(remoteJobs.get(language) == null ? 0 : remoteJobs.get(language));
            //        Key skills
            Map<String, Integer> skillsCount = countKeySkills(db.getKeySkills(language));
            List<String> sortedSkills = getSortedSkills(skillsCount);
//            for (String skill : sortedSkills){
//                System.out.println(skill);
//            }
            languageLine[5] = "\"" + getSkillsString(sortedSkills, 5) + "\"";
            table.add(languageLine);
        }

        return table;
    }
    // Sorting with ~O(n) for a common case
    // TODO: Code can be significantly simplified with sorting the List of entries.
    // TODO: The algorithm is not so fast but it doesn't matter
    private List<String> getSortedSkills(Map<String, Integer> skillsCount){
        int maxCount = 0;
        for (Map.Entry skill : skillsCount.entrySet()){
            if ((int)skill.getValue() > maxCount){
                maxCount = (int)skill.getValue();
            }
        }
        List<String>[] sortedSkills = new List[maxCount];
        Arrays.fill(sortedSkills, null);
        for (Map.Entry skill : skillsCount.entrySet()){
            List<String> element = sortedSkills[(int)skill.getValue()-1];
            if (element == null){
                element = new LinkedList<>();
                element.add((String)skill.getKey());
                sortedSkills[(int)skill.getValue()-1] = element;
            }
            else{
                element.add((String)skill.getKey());
            }
        }
        List<String> skillsList = new LinkedList<>();
        for (int i = sortedSkills.length - 1; i >= 0; i--){
            if (sortedSkills[i] != null) {
                for (String skill : sortedSkills[i]) {
                    skillsList.add(skill);
                }
            }
        }
        return skillsList;
    }
    private Map<String, Integer> countKeySkills(List<String[]> skills){
        Map<String, Integer> skillsCount = new HashMap<>();
        for (String[] skillsArray : skills){
            for (int i = 0; i < skillsArray.length; i++) {
                Integer skillCounter = skillsCount.get(skillsArray[i]);
                int counter;
                if (skillCounter == null) {
                    counter = 1;
                } else {
                    counter = skillCounter + 1;
                }
                skillsCount.put(skillsArray[i], counter);
            }
        }
        return skillsCount;
    }
    private String getSkillsString(List<String> sortedSkills, final int threshold){
        StringBuilder skillsString = new StringBuilder();
        Iterator sortedSkillsIterator = sortedSkills.iterator();
        if (sortedSkills.size() > 0) {
            for (int i = 0; ; i++) {
                skillsString.append(sortedSkillsIterator.next());
                if (i < threshold && sortedSkillsIterator.hasNext()) {
                    skillsString.append(", ");
                } else {
                    break;
                }
            }
        }
        return skillsString.toString();
    }
}
