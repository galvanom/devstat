package com.devstat;

import javax.xml.crypto.Data;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class DataBase {
    private DataBase() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Can't load DB driver\n" + e);
        }
    }
    private static DataBase instance;
    public static DataBase getInstance(){
        if (instance == null){
            synchronized (DataBase.class) {
                if (instance == null) {
                    instance = new DataBase();
                }
            }
        }
        return instance;

    }
    private Connection dbConnection;
    private void establishConnection(){
        try {
            if (dbConnection == null || !dbConnection.isValid(1)) {
                if (dbConnection != null && !dbConnection.isClosed()) {
                    dbConnection.close();
                }
                dbConnection = DriverManager
                        .getConnection("jdbc:postgresql://localhost:5432/devstat", "postgres", "12345678");
            }
        }
        catch(SQLException e){
            System.out.println("Error while establish DB connection " + e.toString());
        }
    }


    public void insert(List<Vacancy> vacancies) {
        Statement statement = null;
        try {
            establishConnection();
            statement = dbConnection.createStatement();
            dbConnection.setAutoCommit(false);
            for (Vacancy vacancy : vacancies) {
                System.out.println(createVacanciesInsertRequest(vacancy));
                System.out.println(createVacanciesUpdateRequest(vacancy));
                System.out.println(createDownloadedInsertRequest(vacancy));
                statement.executeUpdate(createVacanciesUpdateRequest(vacancy));
                statement.executeUpdate(createVacanciesInsertRequest(vacancy));
                statement.executeUpdate(createDownloadedInsertRequest(vacancy));
            }
            dbConnection.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (statement != null){
                try {
                    statement.close();
                }
                catch (SQLException e){
                    System.out.println("Can't close statement " + e.toString());
                }
            }
        }
    }

    public Map<String, String> getExperience(String fromDate, String toDate){
        String query = "SELECT SUM(al.cnt) as sumcnt, al.lang_id as lang, experience FROM " +
                " ("+
                " SELECT COUNT(DISTINCT dl.vacancy_id) as cnt, v.lang_id, experience from vacancies as v , downloaded_list as dl " +
                " WHERE dl.timestamp > \'"+fromDate+"\' AND dl.timestamp < \'"+toDate+"\' "+
                " AND v.vacancy_id = dl.vacancy_id " +
                " GROUP BY v.lang_id, dl.vacancy_id,  experience " +
                ") " +
                " AS al " +
                " GROUP BY lang_id, experience";
        Map<String, String> experience = new HashMap<>();
        establishConnection();
        try (Statement statement = dbConnection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            Map<String, Map.Entry<String, Integer>> maxExp = new HashMap<>();
            while(resultSet.next()){
                String lang = resultSet.getString("lang");
                String exp = resultSet.getString("experience");
                Integer count = resultSet.getInt("sumcnt");
                if (!maxExp.containsKey(lang) || maxExp.get(lang).getValue() < count){
                    maxExp.put(lang, new AbstractMap.SimpleEntry<>(exp, count));
                }
            }
            for (Map.Entry<String, Map.Entry<String,Integer>> max : maxExp.entrySet()){
                experience.put(max.getKey(), max.getValue().getKey());
            }
        }
        catch (SQLException e){
            System.out.println("Can't get experience from " +fromDate+ " to " +toDate+" \n" +
                    e + "\n Query: " + query);
        }
        return experience;
    }

    public Map<String, Integer> getRemoteJobsNumber(String fromDate, String toDate){
        String query = "SELECT SUM(al.cnt) as sumcnt, lang_id as lang FROM " +
                "(" +
                "SELECT COUNT(DISTINCT dl.vacancy_id) as cnt, v.lang_id, schedule from vacancies as v , downloaded_list as dl " +
                "WHERE dl.timestamp > \'"+fromDate+"\' AND dl.timestamp < \'"+toDate+"\'"+
                "AND v.vacancy_id = dl.vacancy_id " +
                "GROUP BY v.lang_id, dl.vacancy_id, schedule " +
                ") " +
                "AS al " +
                "WHERE schedule = \'remote\' " +
                "GROUP BY lang_id";
        Map<String, Integer> remoteJobs = new HashMap<>();
        establishConnection();
        try (Statement statement = dbConnection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String key = resultSet.getString("lang");
                Integer value = resultSet.getInt("sumcnt");
                remoteJobs.put(key,value);
            }
        }
        catch (SQLException e){
            System.out.println("Can't get remote jobs number from " +fromDate+ " to " +toDate+" \n" +
                    e + "\n Query: " + query);
        }
        return remoteJobs;

    }

    public Map<String, Integer> getVacanciesNumber(String fromDate, String toDate){
        String query =  "SELECT COUNT(DISTINCT dl.vacancy_id) as cnt, v.lang_id as lang from vacancies as v , downloaded_list as dl" +
                        " WHERE dl.timestamp > \'"+fromDate+"\' AND dl.timestamp < \'"+toDate+"\'"+
                        " AND v.vacancy_id = dl.vacancy_id" +
                        " GROUP BY v.lang_id";
        Map<String, Integer> vacanciesNumber = new HashMap<>();
        establishConnection();

        try (Statement statement = dbConnection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String key = resultSet.getString("lang");
                Integer value = resultSet.getInt("cnt");
                vacanciesNumber.put(key,value);
            }
        }
        catch (SQLException e){
            System.out.println("Can't get vacancies number from " +fromDate+ " to " +toDate+"\n " + e + "\n Query: " + query);
        }

        return vacanciesNumber;

    }
    public Map<String, Double> getAvgSalary(String fromDate, String toDate){
        String query = "SELECT lang_id as lang, AVG(salary_avg) as savg  FROM " +
                " (SELECT DISTINCT dl.vacancy_id, v.lang_id, salary_avg from vacancies as v , downloaded_list as dl" +
                "  WHERE dl.timestamp > \'"+fromDate+"\' AND dl.timestamp < \'"+toDate+"\'" +
                "  AND v.vacancy_id = dl.vacancy_id AND salary_avg > 0 AND salary_currency =\'RUR\') as salary" +
                "  GROUP BY lang_id";
        Map<String, Double> avgSalary = new HashMap<>();
        establishConnection();

        try (Statement statement = dbConnection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String key = resultSet.getString("lang");
                Double value = resultSet.getDouble("savg");
                avgSalary.put(key,value);
            }
        }
        catch (SQLException e){
            System.out.println("Can't get avgSalary from " +fromDate+ " to " +toDate+"\n " + e+ " \nQuery: " + query);
        }
        return avgSalary;
    }
    List<String[]> getKeySkills(String lang_id){
        String query = "SELECT key_skills FROM vacancies WHERE lang_id = \'"+lang_id+"\' AND key_skills <> \'{}\'";
        List<String[]> result = new ArrayList<>();
        establishConnection();
        try(Statement statement = dbConnection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                result.add((String[])resultSet.getArray("key_skills").getArray());
            }
        }
        catch (SQLException e){
            System.out.println("Can't get key skills on " + lang_id + " \n" + e+ "\n Query: " + query);
        }
        catch (ClassCastException e){
            System.out.println("Can't cast key skills on " + lang_id + " \n" + e+ "\n Query: " + query);
        }
        return result;
    }
    

    private String createVacanciesUpdateRequest(Vacancy vacancy){
        return "UPDATE vacancies SET vacancy_name = $$" + vacancy.getVacancyName()+"$$ " +
                ", key_skills = $$" + skillsToPostgresArray(vacancy.getKeySkills()) + "$$" +
                ", employer_id = " + vacancy.getEmployerId() + " " +
                ", employer_name = $$" + vacancy.getEmployerName() + "$$ " +
                ", specialization_id = " + vacancy.getSpecializationId() + " " +
                ", area_id = " + vacancy.getArea() + " " +
                ", salary_to = " + vacancy.getSalaryTo() + " " +
                ", salary_from = " + vacancy.getSalaryFrom() + " " +
                ", salary_avg = " + vacancy.getSalaryAvg() + " " +
                ", salary_currency = \'" + vacancy.getSalaryCurrency() + "\' " +
                ", experience = \'" + vacancy.getExperience() + "\' " +
                ", schedule = \'" + vacancy.getSchedule() + "\' " +
                " WHERE vacancy_id = " + vacancy.getVacancyId() +
                " AND lang_id = \'" + vacancy.getLangName() + "\'" + " ;";
    }
    private String createVacanciesInsertRequest(Vacancy vacancy){
        return "INSERT INTO vacancies " +
                "(vacancy_id, vacancy_name, lang_id, key_skills, employer_id, employer_name, specialization_id, area_id," +
                " salary_to, salary_from, salary_avg, salary_currency, experience, schedule)" +
                " SELECT " + vacancy.getVacancyId() +
                ", $$" + vacancy.getVacancyName()+"$$" +  ", \'" + vacancy.getLangName() + "\'" +
                ", $$" + skillsToPostgresArray(vacancy.getKeySkills()) + "$$" +
                ", " + vacancy.getEmployerId() + " " +
                ", $$" + vacancy.getEmployerName() + "$$ " +
                ", " + vacancy.getSpecializationId() + " " +
                ", " + vacancy.getArea() + " " +
                ", " + vacancy.getSalaryTo() + " " +
                ", " + vacancy.getSalaryFrom() + " " +
                ", " + vacancy.getSalaryAvg() + " " +
                ", \'" + vacancy.getSalaryCurrency() + "\' " +
                ", \'" + vacancy.getExperience() + "\' " +
                ", \'" + vacancy.getSchedule() + "\' " +
                " WHERE NOT EXISTS ( SELECT 1 FROM vacancies " +"WHERE vacancy_id = " + vacancy.getVacancyId() +
                " AND lang_id = \'" + vacancy.getLangName() + "\'" + " );";
    }
    private String createDownloadedInsertRequest(Vacancy vacancy){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = sdf.format(new Date());
        return "INSERT INTO downloaded_list (vacancy_id, lang_id, timestamp) VALUES (" +
                vacancy.getVacancyId() + ", \'" + vacancy.getLangName()+"\'" + ", \'" + timestamp  +"\' ) ;";
    }
    public String skillsToPostgresArray(ArrayList<String> skills){
        StringBuilder postgresArray = new StringBuilder();
        postgresArray.append("{");
        int counter = 0;
        for (String skill : skills){
            postgresArray.append("\"");
            postgresArray.append(skill);
            postgresArray.append("\"");
            counter++;
            if (counter < skills.size()){
                postgresArray.append(",");
            }
        }
        postgresArray.append("}");

        return postgresArray.toString();
    }
}
